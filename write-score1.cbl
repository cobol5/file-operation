       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-SCORE1.
       AUTHOR. JIRAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  SCORE-FILE.
       01 SCORE-DETAILS.
          05 STU-ID         PIC 9(8).
          05 MIDTERM-SCORE  PIC 9(2)V9(2).
          05 FINAL-SCORE    PIC 9(2)V9(2).
          05 PROJECT-SCORE  PIC 9(2)V9(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE
           MOVE "62160249" TO STU-ID
           MOVE "37.05" TO MIDTERM-SCORE 
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "13.5" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS 

           MOVE "62160294" TO STU-ID
           MOVE "20" TO MIDTERM-SCORE 
           MOVE "20" TO FINAL-SCORE 
           MOVE "10" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS 

           MOVE "62160006" TO STU-ID
           MOVE "30.8" TO MIDTERM-SCORE 
           MOVE "40" TO FINAL-SCORE 
           MOVE "14.25" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS

           MOVE "62160310" TO STU-ID
           MOVE "10.8" TO MIDTERM-SCORE 
           MOVE "20" TO FINAL-SCORE 
           MOVE "4.25" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS  

           MOVE "62160055" TO STU-ID
           MOVE "40" TO MIDTERM-SCORE 
           MOVE "40" TO FINAL-SCORE 
           MOVE "20" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS  
           CLOSE SCORE-FILE 
           GOBACK 
           .
           